package main

import (
	"io"

	"gitea.com/lunny/tango"
)

func main() {
	t := tango.Classic()
	t.Get("/", func(t *tango.Context) {
		io.WriteString(t, "hello tango")
	})
}
